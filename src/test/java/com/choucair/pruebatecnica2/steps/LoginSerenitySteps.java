package com.choucair.pruebatecnica2.steps;

import com.choucair.pruebatecnica2.utilities.AccionesWeb;

import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Step;

import java.util.List;

import com.choucair.pruebatecnica2.pageobjects.LoginSerenityPage;

public class LoginSerenitySteps {

	LoginSerenityPage loginserenitypage;
	AccionesWeb accionesWeb;

	@Step
	public void ingresar_datos_login(String strusuario, String strclave) {
		accionesWeb.abrirURL("https://serenity.is/demo/Account/Login/?ReturnUrl=%2Fdemo%2F");
		accionesWeb.bordearElemento(loginserenitypage.txtusuario);
		accionesWeb.clear_sendKeys(loginserenitypage.txtusuario, strusuario);
		accionesWeb.bordearElemento(loginserenitypage.txtclave);
		accionesWeb.clear_sendKeys(loginserenitypage.txtclave, strclave);
		accionesWeb.esperasegundos(2);
		accionesWeb.bordearElemento(loginserenitypage.txtingresar);
		accionesWeb.click(loginserenitypage.txtingresar, false);
	}

}
