package com.choucair.pruebatecnica2.steps;

import java.util.List;

import com.choucair.pruebatecnica2.pageobjects.MeetSerenityPage;
import com.choucair.pruebatecnica2.utilities.AccionesWeb;

import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Step;

public class MeetSerenitySteps {
	
	MeetSerenityPage meetserenitypage;
	AccionesWeb accionesWeb;
	
	@Step
	public void crear_meet() {
		accionesWeb.bordearElemento(meetserenitypage.txtmenumeet);
		accionesWeb.click(meetserenitypage.txtmenumeet, false);
		accionesWeb.bordearElemento(meetserenitypage.txtmeet);
		accionesWeb.click(meetserenitypage.txtmeet, false);
		accionesWeb.bordearElemento(meetserenitypage.txtnuevomeet);
		accionesWeb.click(meetserenitypage.txtnuevomeet, false);
	}

	@Step
	public void diligenciar_datos_meet_nombre(String datoPruebanombre) {
		accionesWeb.bordearElemento(meetserenitypage.txtmeetingname);
		meetserenitypage.txtmeetingname.click();
		meetserenitypage.txtmeetingname.clear();
		meetserenitypage.txtmeetingname.sendKeys(datoPruebanombre);

	}

	@Step
	public void diligenciar_datos_meet_numero(String datoPruebanumero) {
		accionesWeb.bordearElemento(meetserenitypage.txtmeetingnumber);
		meetserenitypage.txtmeetingnumber.click();
		meetserenitypage.txtmeetingnumber.clear();
		meetserenitypage.txtmeetingnumber.sendKeys(datoPruebanumero);
	}

	@Step
	public void diligenciar_datos_meet_unit(String datounit) {
		accionesWeb.bordearElemento(meetserenitypage.txtmeetingunit);
		meetserenitypage.txtmeetingunit.click();
		// loginserenitypage.txtmeetingunit.selectByVisibleText(datounit);
		String strnombreunidad = Serenity.sessionVariableCalled("VS_BusinessUnit");
		accionesWeb.click(meetserenitypage.strXptOpcionUnidad.replace("REEMPLAZAR", strnombreunidad), false);
		accionesWeb.bordearElemento(meetserenitypage.txtguardarmeet);
		accionesWeb.click(meetserenitypage.txtguardarmeet, false);
		accionesWeb.bordearElemento(meetserenitypage.txtregresar);
		accionesWeb.click(meetserenitypage.txtregresar, false);
	}

	@Step
	public void diligenciar_datos_meet2(List<List<String>> data, int id) {
		diligenciar_datos_meet_nombre(data.get(id).get(0).trim());
		diligenciar_datos_meet_numero(data.get(id).get(1).trim());
	}

	@Step
	public void verificar() {
		accionesWeb.bordearElemento(meetserenitypage.txtid);
		accionesWeb.click(meetserenitypage.txtid, false);
		accionesWeb.esperasegundos(4);
	}
	
	}

