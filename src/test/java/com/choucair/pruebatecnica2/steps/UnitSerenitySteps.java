package com.choucair.pruebatecnica2.steps;

import com.choucair.pruebatecnica2.pageobjects.UnitSerenityPage;
import com.choucair.pruebatecnica2.utilities.AccionesWeb;

import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Step;

public class UnitSerenitySteps {

	UnitSerenityPage unitserenitypage;
	AccionesWeb accionesWeb;
	
	@Step //
	public void crear_business_unit(String strnombreunidad) {
		accionesWeb.bordearElemento(unitserenitypage.txtmenuorganizacional);
		accionesWeb.click(unitserenitypage.txtmenuorganizacional, false);
		accionesWeb.bordearElemento(unitserenitypage.txtmenubusiness);
		accionesWeb.click(unitserenitypage.txtmenubusiness, false);
		accionesWeb.bordearElemento(unitserenitypage.txtnewunidad);
		accionesWeb.click(unitserenitypage.txtnewunidad, false);
		accionesWeb.bordearElemento(unitserenitypage.txtnombrebusines);
		accionesWeb.clear_sendKeys(unitserenitypage.txtnombrebusines, strnombreunidad);
		accionesWeb.bordearElemento(unitserenitypage.txtguardar);
		accionesWeb.click(unitserenitypage.txtguardar, false);
		Serenity.setSessionVariable("VS_BusinessUnit").to(strnombreunidad);

	}
}
