package com.choucair.pruebatecnica2.definitions;

import java.util.List;

import com.choucair.pruebatecnica2.steps.LoginSerenitySteps;
import com.choucair.pruebatecnica2.steps.MeetSerenitySteps;
import com.choucair.pruebatecnica2.steps.UnitSerenitySteps;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class LoginSerenityDefinitions {
	@Steps
	LoginSerenitySteps loginserenitysteps;
	@Steps
	UnitSerenitySteps unitSerenitySteps;
	@Steps
	MeetSerenitySteps meetSerenitySteps;

	@Given("^accedo al formulario del login de serenity con usuario \"([^\"]*)\" y contraseña \"([^\"]*)\"$")
	public void accedo_al_formulario_del_login_de_serenity_con_usuario_y_contraseña(String strusuario,
			String strclave) {
		loginserenitysteps.ingresar_datos_login(strusuario, strclave);
	}

	@When("^ingreso en el menu organizacion creo una nueva Business Unit con la palabra \"([^\"]*)\"$")
	public void ingreso_en_el_menu_organizacion_creo_una_nueva_Business_Unit_con_la_palabra(String strnombreunidad) {
		unitSerenitySteps.crear_business_unit(strnombreunidad);
	}

	@When("^se crea un nuevo meet con todos los datos solicitados$")
	public void se_crea_un_nuevo_meet_con_todos_los_datos_solicitados(DataTable Dt) {
		meetSerenitySteps.crear_meet();
		List<List<String>> data = Dt.raw();
		for (int i = 1; i < data.size(); i++) {
			meetSerenitySteps.diligenciar_datos_meet2(data, i);
		}
		meetSerenitySteps.diligenciar_datos_meet_unit(null);

	}

	@Then("^validar la reunion creada$")
	public void validar_la_reunion_creada() {
		meetSerenitySteps.verificar();
	}

}
