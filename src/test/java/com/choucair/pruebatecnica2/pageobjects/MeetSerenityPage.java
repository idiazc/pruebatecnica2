package com.choucair.pruebatecnica2.pageobjects;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class MeetSerenityPage extends PageObject{

	
	// seleccionar menu de meet
		@FindBy(xpath = "//i[@class='nav-icon fa fa-comments premium-feature']")
		public WebElementFacade txtmenumeet;

		// Seleccionar submenu meet
		@FindBy(xpath = "//a[@href='/demo/Meeting/Meeting']")
		public WebElementFacade txtmeet;

		// Crear meet
		@FindBy(xpath = "//i[@class='fa fa-plus-circle text-green']")
		public WebElementFacade txtnuevomeet;

		// boton meeting name
		@FindBy(xpath = "//input[@name='MeetingName']")
		public WebElementFacade txtmeetingname;

		// boton meeting number
		@FindBy(xpath = "//input[@name='MeetingNumber']")
		public WebElementFacade txtmeetingnumber;

		// boton meeting unit
		@FindBy(xpath = "(//a[@href='javascript:void(0)'])[8]")
		public WebElementFacade txtmeetingunit;

		// boton unit
		public String strXptOpcionUnidad = "(//div[@class='select2-result-label' and contains(text(),'REEMPLAZAR')])[1]";

		// boton guardar
		@FindBy(xpath = "//i[@class='fa fa-check-circle text-purple']")
		public WebElementFacade txtguardarmeet;
		
		//boton regresar
		@FindBy (xpath="//button[@class='panel-titlebar-close']")
		public WebElementFacade txtregresar;
		
		//boton id
		@FindBy (xpath="//span[@class='slick-column-name']")
		public WebElementFacade txtid;
		
}
