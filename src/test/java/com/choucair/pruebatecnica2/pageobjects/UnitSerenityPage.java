package com.choucair.pruebatecnica2.pageobjects;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class UnitSerenityPage extends PageObject {

	
		// boton menu organizacional
		@FindBy(xpath = "//i[@class='nav-icon fa fa-sitemap premium-feature']")
		public WebElementFacade txtmenuorganizacional;

		// boton submenu business
		@FindBy(xpath = "//a[@href='/demo/Organization/BusinessUnit']")
		public WebElementFacade txtmenubusiness;

		// crear nueva business
		@FindBy(xpath = "//i[@class='fa fa-plus-circle text-green']")
		public WebElementFacade txtnewunidad;

		// nombre business
		@FindBy(xpath = "//input[@name='Name']")
		public WebElementFacade txtnombrebusines;

		// seleccionar parent
		@FindBy(xpath = "//i[@class='fa fa-floppy-o text-purple']")
		public WebElementFacade txtguardar;

}
