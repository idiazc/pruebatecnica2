package com.choucair.pruebatecnica2.pageobjects;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class LoginSerenityPage extends PageObject {

	// label usuario
	@FindBy(xpath = "//input[@name='Username']")
	public WebElementFacade txtusuario;

	// label clave
	@FindBy(xpath = "//input[@name='Password']")
	public WebElementFacade txtclave;

	// boton ingresar
	@FindBy(xpath = "//button[@id='StartSharp_Membership_LoginPanel0_LoginButton']")
	public WebElementFacade txtingresar;

	
	

}
